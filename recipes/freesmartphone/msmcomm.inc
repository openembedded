DESCRIPTION = "Low level protocol implementation for binary protocol spoken by some Qualcomm modems"
HOMEPAGE = "http://www.freesmartphone.org"
AUTHOR = "Simon Busch <morphis@gravedo.de>"
SECTION = "console/network"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=59530bdf33659b29e73d4adb9f9f6552"
INC_PR = "r4"
PV = "Please override!"

SRCREV = "8cea4baf7656b1a2be973272bfdd524e367b259b"
SRC_URI = "${FREESMARTPHONE_GIT}/msmcomm.git;protocol=git;branch=master"

